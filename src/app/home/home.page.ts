import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  age: number;
  upper: number;
  lower: number;

  constructor() {}

  calculate() {
    this.upper = (220 - this.age) * 0.85;
    this.lower = (220 - this.age) * 0.65;
    this.age = 0;
  }

}
